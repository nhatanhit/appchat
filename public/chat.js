window.onload = function() {
	
	//first need to check the voice is available in google chrome
	if ('speechSynthesis' in window) {
		var messages = [];
		var socket = io.connect('http://10.84.1.172:3700');
		var field = document.getElementById("field");
		var sendButton = document.getElementById("send");
		var content = document.getElementById("content");

		socket.on('message', function (data) {
			if(data.message) {
				messages.push(data.message);
				var html = '';
				for(var i=0; i<messages.length; i++) {
					html += messages[i] + '<br />';
				}
				content.innerHTML = html;
				onloadVoice(data.message);
			} else {
				console.log("There is a problem:", data);
			}	
		});

		sendButton.onclick = function() {
			var text = field.value;
			socket.emit('send', { message: text });
		};
		onloadVoice = function(message) {
			var msg = new SpeechSynthesisUtterance(message);
			window.speechSynthesis.speak(msg);
		}
		
	}
	else {
		alert("Sorry, your browser is not support for read text")
	}
    

}